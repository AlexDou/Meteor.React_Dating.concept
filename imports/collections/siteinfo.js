import { Mongo } from 'meteor/mongo';

const SiteInfo = new Mongo.Collection('siteinfo');

export default SiteInfo;
