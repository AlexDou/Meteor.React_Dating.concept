export const loadImage = (src, callback) => {
	var image = new Image();
	image.onload = function(e) {
		callback(image);
		image = null;
	};

	image.src = src;
}

export const cropImage = (imgDest, imgSrc, crop) => {
	this.loadImage(imgSrc, cropAfterLoad.bind(this));

	const cropAfterLoad = (loadedImg) => {
		var imageWidth = loadedImg.naturalWidth;
		var imageHeight = loadedImg.naturalHeight;

		var cropX = (crop.x / 100) * imageWidth;
		var cropY = (crop.y / 100) * imageHeight;

		var cropWidth = (crop.width / 100) * imageWidth;
		var cropHeight = (crop.height / 100) * imageHeight;

		var canvas = document.createElement('canvas');
		canvas.width = cropWidth;
		canvas.height = cropHeight;
		var ctx = canvas.getContext('2d');

		ctx.drawImage(loadedImg, cropX, cropY, cropWidth, cropHeight, 0, 0, cropWidth, cropHeight);

        if (HTMLCanvasElement.prototype.toBlob) {
          canvas.toBlob(function(blob) {
            var url = URL.createObjectURL(blob);

            imgDest.onload = function() {
              URL.revokeObjectURL(url);
              this.ready();
            };

            imgDest.src = url;
          });
        } else {
          imgDest.src = canvas.toDataURL('image/jpeg');
        }
	}
}
