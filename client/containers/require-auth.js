import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

export default function(ComposedComponent) {
  class Authentication extends Component {

    componentWillMount() {
      if (!this.props.auth) {
        FlowRouter.go('/');
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.auth) {
        FlowRouter.go('/');
      }
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  return createContainer(() => {
      return {
          auth: Meteor.userId()
      };
  }, Authentication);
}
