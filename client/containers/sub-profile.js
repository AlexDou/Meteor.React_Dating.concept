import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import Images64 from '../../imports/collections/images64';
import FIELDS from '../../imports/lib/fields';

export default function(ComposedComponent) {
  class subProfile extends Component {

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  return createContainer(() => {
      Meteor.subscribe("profile");
      Meteor.subscribe("photo");

      const profile = Meteor.users.find({ _id:Meteor.userId() }, {fields: FIELDS}).fetch();
      let photo = Images64.find({ userId:Meteor.userId() }).fetch();
      if (photo && photo.length) {
        photo = photo[0]['photo'];
      }

      return {
          profile,
          photo
      };
  }, subProfile);
}
