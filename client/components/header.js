import React, { Component } from 'react';
import LoginForm from './accounts';

class Header extends Component {
  /*onItemClick(event) {
    event.preventDefault();

    Meteor.call('bins.insert', (error, binId) => {
      browserHistory.push(`/bins/${binId}`);
    });
}*/

extRootLink() {
    return (
        <div className="navbar-header">
            <a href={FlowRouter.path('root.route')} className="navbar-brand"> Teezdom Come </a>
        </div>
    )
}

extLogin() {
    return (
        <li>
            <LoginForm />
        </li>
    )
}

intDiscover() {
    return (
        <li className="internal-menu-item">
            <a href={FlowRouter.path('discover')}>Discover</a>
        </li>
    )
}

intProfile() {
    return (
        <li className="internal-menu-item">
            <a href={FlowRouter.path('profile')}>Profile</a>
        </li>
    )
}

  render() {
    return (
      <nav className="nav navbar-default">
        {this.extRootLink()}
        <ul className="nav navbar-nav">
            {Meteor.userId() ? this.intDiscover() : ''}
            {this.extLogin()}
            {Meteor.userId() ? this.intProfile() : ''}
        </ul>
      </nav>
    );
  }
}

export default Header;
