import React, { Component } from 'react';
import Header from './header';

export default class App extends Component{
  render() {
    return (
        <div>
            <Header />
            <div className="app">
              {this.props.content}
            </div>
            <div className="footer">
              Sample dating app based on Meteor/React
            </div>
        </div>
    );
  }
};
