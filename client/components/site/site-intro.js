import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import SiteInfo from '../../../imports/collections/siteinfo';

class SiteInfoRender extends Component {
    render() {
      const { title, intro } =
        (typeof this.props.siteinfo[0] !== 'undefined') ?
        this.props.siteinfo[0] :
        '';

        return (
            <div className="container">
                <div className="jumbotron">
                    <h3>{title}</h3>
                    <p dangerouslySetInnerHTML={{ __html: intro }} />
                </div>
            </div>
        )
    }
}

export default createContainer(() => {
  Meteor.subscribe('siteinfo');
  return { siteinfo: SiteInfo.find({}).fetch() };
}, SiteInfoRender);
