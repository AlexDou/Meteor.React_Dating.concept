import React, {Component, PropTypes} from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import Images64 from '../../../imports/collections/images64';
import {EJSON} from 'meteor/ejson';

import Single from './user-single';

// toDo: page navigation

class UserListComponent extends Component{
    userList() {
        if (this.props.userSet.length) {
            return this.props.userSet.map((user) =>
                <Single user={user} photo={this.props.userSetPhotos[user.profile.photoId] || null} key={user._id} />
            )
        } else {
            return (
                <div className="row">
                    <div className="col-xs-8 col-sm-offset-3">
                        There are no users for you to discover
                    </div>
                </div>
            )
        }
    }

    render() {
        if (this.props.userSet) {
            return (
                <div>
                    {this.userList()}
                </div>
            )
        }

        return (
            <div>
                Loading..
            </div>
        )
    }
}

export default UserList = createContainer(() => {
    const user = Meteor.user();
    if (user) {
        const gender = (user.profile.gender === 'f') ? 'm' : 'f';
        const subUserList = Meteor.subscribe("user.list", gender, 1); // hardcoded 1st page for now
        if (subUserList.ready()) {
            const userSet = Meteor.users.find(
                {_id: { $ne: user._id },'profile.gender': gender}
            ).fetch();

            const userPhotoIds = userSet.map((user) => {
                if (user.profile.photoId) {
                    return user.profile.photoId
                }
            });

            if (userPhotoIds.length) {
                const subUserListPhotos = Meteor.subscribe('user.list.photos', userPhotoIds, 1);
                if (subUserListPhotos.ready()) {
                    let userSetPhotos = {};
                    const userListPhotos = Images64.find({_id: {$in: userPhotoIds}}).fetch();
                    userListPhotos.map((photo) => {
                        userSetPhotos[photo._id] = photo.photo;
                    });
                    return { userSet, userSetPhotos };
                }
            } else {
                return { userSet, userSetPhotos: [] };
            }
        }
    }
    return {};
}, UserListComponent);
