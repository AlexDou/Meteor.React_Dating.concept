import React from 'react';
import { EJSON } from 'meteor/ejson';

// toDo: install and use http://react-component.github.io/collapse/

const Single = ({ user, photo }) => {

    const insta = user.profile.insta ? EJSON.parse(user.profile.insta) : null;
    return (
        <div className="row">
            <div className="col-sm-6 col-sm-offset-3">
                <p className="user-name">
                    {`${user.profile['first-name']} ${user.profile['last-name']}`}
                </p>
            </div>
            {photo ? <div className="col-sm-6">
                <h4>Profile Photo</h4>
                <div className="profile-photo">
                    <img src={photo} />
                </div>
            </div> : '' }
            {(insta && insta.length) ? <div className="col-sm-12">
                <h4>more Photos</h4>
                {insta.map((img, idx) =>
                    <div className="recent-media-item recent-media-user-list" key={idx}>
                        <img src={img} />
                    </div>
                )}
            </div> : '' }
            <div className="col-sm-6 col-sm-offset-3">
                <h4>Contacts</h4>
                <p>Email: {user.emails[0]['address']}</p>
            </div>
            <div className="col-sm-12">
                <h4>About</h4>
                <p>
                    {user.profile.about ? user.profile.about : ''}
                </p>
            </div>
        </div>
    )
}

export default Single;
