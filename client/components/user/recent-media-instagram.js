import React, { Component } from 'react';
import _ from 'underscore';
import { EJSON } from 'meteor/ejson';

export default class RecentMediaInstagram extends Component {

    constructor(props) {
        super(props);

        this.state = {
            insta: [],
            active: []
        };
    }

    componentWillMount() {
        this.setState({
            insta: this.props.insta || []
        });
    }

    componentWillReceiveProps() {
        if (!this.state.insta.length) {
            this.setState({
                insta: this.props.insta || []
            });
        }
    }

    handleChange(ele) {
        const pos = _.indexOf(this.state.active, ele.target.getAttribute('data-src'));
        if (ele.target.checked &&  pos < 0) {
            this.setState({
                active: [...this.state.active, ele.target.getAttribute('data-src')]
            })
        }
        if (!ele.target.checked && _.indexOf(this.state.active, ele.target.getAttribute('data-src')) >= 0) {
            this.setState({
                active: [...this.state.active.slice(0, pos), ...this.state.active.slice(pos+1)]
            })
        }
    }

    handleRecent() {
        const jsonActive = EJSON.stringify(this.state.active);
        Meteor.call('update.recent.instagram', jsonActive, (err, res) => {
            if (err) {
                document.querySelector('.success').innerHTML = '';
                document.querySelector('.error').innerHTML = `Error on update ${EJSON.stringify(err)}`;
            }
            else {
                //Array.prototype.slice.call(
                //    document.querySelectorAll('input')
                //)
                [...document.querySelectorAll('input')].map(
                    (inp) => {inp.checked = false;}
                );

                this.setState({
                    insta: EJSON.parse(jsonActive),
                    active: []
                })
            }
        });
    }

    render() {
        return (
            <div>
                <div className="row insta-block">
                    {(this.state.insta && this.state.insta.length) ? this.state.insta.map((img, idx) =>
                        <div className="recent-media-item" key={idx}>
                            <img src={img} /><br />
                            <input type="checkbox" data-src={img} onChange={this.handleChange.bind(this)} /> Persist this Photo
                        </div>
                    ) : ''}
                </div>
                {(this.state.insta && this.state.insta.length) ? <div className="row insta-submit">
                    <button type="button" className="btn btn-default" onClick={this.handleRecent.bind(this)}>
                        Update My Photos Collection
                    </button>
                </div> : ''}
            </div>
        )
    }
}
