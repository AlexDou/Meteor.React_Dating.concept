import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Form, Text, Checkbox, RadioGroup, Radio, FormError } from 'react-form';
import RecentMediaInstagram from './recent-media-instagram';
import { EJSON } from 'meteor/ejson';
import FIELDS from '../../../imports/lib/fields-insta';

class ProfileInstagram extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            recent: [],
            recentReady: null
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            recent: nextProps.recent || [],
            recentReady: nextProps.recentReady
        });
    }

    handleChange(values) {
        this.setState({
            username: values.username,
            password: values.password
        });
    }

    handleSubmit() {
        this.setState({
            recent: [],
            recentReady: null
        });
        Meteor.call('profile.instagram', EJSON.stringify(this.state), (err, res) => {
            if (err) {
                document.querySelector('.success').innerHTML = '';
                document.querySelector('.error').innerHTML = `Error on update ${EJSON.stringify(err)}`;
            }
            else {
                if (typeof res === 'object' && res.length) {
                    Meteor.call('update.recent.instagram', EJSON.stringify(res), (err, imgs) => {
                        if (err) {
                            document.querySelector('.success').innerHTML = '';
                            document.querySelector('.error').innerHTML = `Error on update ${EJSON.stringify(err)}`;
                        } else {
                            this.setState({
                                recentReady: 1,
                                recent: res
                            })
                        }
                    });
                }
            }
        });
    }

    render() {
        return (
            <div className="row">
                <div className="col-xs-8 col-sm-offset-2">
                    <Form
                        onValidationFail = {(state, props) => {
                            console.log('state', state)
                        }}
                        onSubmit = {(values, state, props) => {
                            (this.handleSubmit.bind(this))();
                        }}
                        onChange = {(state, props, initial=false) => {
                            if (Object.keys(state.touched) && Object.keys(state.touched).length) {
                                (this.handleChange.bind(this, state.values))();
                            }
                        }}
                        validate={({ username, password }) => {
                          return {
                            username: (!username || !username.match(/^[a-z0-9\._\-]+$/i) || username.length < 4) ?
                                    'Username is invalid' :
                                    undefined,
                            password: (!password || password.length < 6) ?
                                    'Invalid password' :
                                    undefined
                          }
                        }}
                      >
                        {({ values, props, submitForm, getValue, getError, touched }) => {
                          return (
                            <div className="container">
                                <div className="error"></div>
                                <div className="success"></div>
                                <div className="well well-sm">
                                    <form onSubmit={submitForm}>
                                        <div className="form-group">
                                            <label>Instagram Username</label>
                                            <Text field='username' className="form-control" />
                                        </div>
                                        <div className="form-group">
                                            <label>Instagram Password (¡demo only!)</label>
                                            <Text type="password" field='password' className="form-control" />
                                        </div>
                                        <p></p>
                                        <div className="btn-group">
                                            <button type='submit' className="btn btn-primary">Submit</button>
                                            <button type='reset' className="btn btn-warning">Reset</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                          )
                        }}
                    </Form>
                </div>
                {this.state.recentReady ?
                <RecentMediaInstagram insta={this.state.recent} /> : '' }
            </div>
        )
    }
}

export default ProfileConnectInstagram = createContainer(() => {
    const insta_sub = Meteor.subscribe("profile.instagram.recent");

    const insta_doc = Meteor.users.find({_id:Meteor.userId()}, {fields:FIELDS}).fetch();
    let recent = [];

    if (insta_doc) {
        if (insta_doc[0] && insta_doc[0].profile.insta) {
            recent = EJSON.parse(insta_doc[0].profile.insta);
        }
    }

    return {
        recentReady: insta_sub.ready(),
        recent
    };
}, ProfileInstagram);
