import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Form, Text, Checkbox, RadioGroup, Radio, FormError } from 'react-form';
import Dropzone from 'react-dropzone';
import ReactCrop from 'react-image-crop';
import { EJSON } from 'meteor/ejson';

class ProfileEdit extends Component {

    constructor(props) {
        super(props);

        this.state = {
            photo: '',
            photoStatus: [],
            email: '',
            firstName: '',
            lastName: '',
            location: '',
            lang: '',
            gender: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        React.findDOMNode = ReactDOM.findDOMNode;

        MAX_IMG_WIDTH = MAX_IMG_HEIGHT = 260;
        MIN_DIM = 100;
    }

    handleChange(values) {
        this.setState({
            email: values.email,
            firstName: values.firstName,
            lastName: values.lastName,
            location: values.location,
            lang: values.lang,
            gender: values.gender
        });
    }

    onOpenClick() {
      this.dropzone.open();
    }

    onDrop(photo) {
        //let reader = new FileReader();
        //reader.onload = () => {
        document.querySelector('.crop-img').src = '';
        let img = new Image();
        img.src = photo[0].preview;
        img.onload = () => {
            let photoStatus;
            if (img.width < MIN_DIM || img.height < MIN_DIM) {
                photoStatus = [
                    <span className="photo-err">Error: Min size 100x100px</span>, 0, 0
                ]
                this.setState({
                    photo: '',
                    photoStatus
                })
            }
            else if (img.width > MAX_IMG_WIDTH) {
                photoStatus = [
                    <span className="photo-scs">Accepted</span>, MAX_IMG_WIDTH, 'auto'
                ];
                this.setState({
                    photo: photo[0],
                    photoStatus
                })
            }
            else if (img.height > MAX_IMG_HEIGHT) {
                photoStatus = [
                    <span className="photo-scs">Accepted</span>, 'auto', MAX_IMG_HEIGHT
                ]
                this.setState({
                    photo: photo[0],
                    photoStatus
                })
            }
        }
        //}
    }

    loadImage(src, callback) {
    	var image = new Image();
        image.src = src.preview;
    	image.onload = function(e) {
            callback(image);
    		image = null;
    	};
    }

    scale(options) {
        var scale = options.scale ||
            Math.min(options.maxWidth/options.width, options.maxHeight/options.height);

        scale = Math.min(scale, options.maxScale || 1);

        return {
            scale: scale,
            width: options.width * scale,
            height: options.height * scale
        };
    }

    cropImage(imgDest, imgSrc, crop) {
        this.loadImage(imgSrc, cropAfterLoad.bind(this));

        function cropAfterLoad (loadedImg) {
            const imageWidth = loadedImg.naturalWidth;
            const imageHeight = loadedImg.naturalHeight;

            const cropX = (crop.x / 100) * imageWidth;
            const cropY = (crop.y / 100) * imageHeight;

            const cropWidth = (crop.width / 100) * imageWidth;
            const cropHeight = (crop.height / 100) * imageHeight;

            let destWidth = cropWidth;
            let destHeight = cropHeight;

            if (MAX_IMG_WIDTH || MAX_IMG_HEIGHT) {
                // Scale the crop.
                const scaledCrop = this.scale({
                    width: cropWidth,
                    height: cropHeight,
                    maxWidth: MAX_IMG_WIDTH,
                    maxHeight: MAX_IMG_HEIGHT
                });

                destWidth = scaledCrop.width;
                destHeight = scaledCrop.height;
            }

            var canvas = document.createElement('canvas');
            canvas.width = destWidth;
            canvas.height = destHeight;
            var ctx = canvas.getContext('2d');

            ctx.drawImage(loadedImg, cropX, cropY, cropWidth, cropHeight, 0, 0, destWidth, destHeight);

            imgDest.src = canvas.toDataURL('image/jpeg');
        }
    }

    onCropComplete(cropImg) {
      this.cropImage(document.querySelector('.crop-img'), this.state.photo, cropImg);
    }

    handleSubmit() {
        const updateObj = Object.assign(this.state, {photo: document.querySelector('.crop-img').src});
        Meteor.call('profile.update', EJSON.stringify(updateObj), (err, res) => {
            if (err) {
                document.querySelector('.success').innerHTML = '';
                document.querySelector('.error').innerHTML = `Error on update ${EJSON.stringify(err)}`;
            }
            else {
                document.querySelector('.success').innerHTML = 'Successfully updated';
                document.querySelector('.error').innerHTML = '';
                document.querySelector('.crop-img').src = '';
                this.setState({
                    photoStatus: ['Change Photo', 0, 0]
                })
            }
        });
    }

    render() {
        if (this.props.profile[0]) {
            if (this.state.email.length < 1) {
                const { emails, profile } = this.props.profile[0];
                const photo = this.props.photo;
                this.state = {
                    photo: photo || '',
                    photoStatus: [(photo && photo.length) ? 'Change Photo' : 'Drop Photo', 0, 0],
                    email: emails[0]['address'] || '',
                    firstName: profile['first-name'] || '',
                    lastName: profile['last-name'] || '',
                    location: profile['location'] || '',
                    lang: profile['lang'] || '',
                    gender: profile['gender'] || ''
                };
            }

            const crop = {
                width: 40,
                aspect: 1,
                minWidth: 100,
                maxWidth: 400
            }

            return (
                <div className="row">
                    <div className="col-xs-8 col-sm-offset-2">
                        {(this.props.photo && typeof this.props.photo !== 'object') ? <div className="user-photo">
                            <img src={this.props.photo} />
                        </div> : null
                        }
                        <div className="dropzone-block">
                            <Dropzone
                                ref={(node) => { this.dropzone = node }}
                                disableClick={false}
                                accept="images/*"
                                multiple={false}
                                minSize="200"
                                maxSize="1240"
                                onDrop={this.onDrop.bind(this)}>
                              <div>{this.state.photoStatus[0]}</div>
                            </Dropzone>
                            <div className="dropzone-btn">
                                <button type="button" onClick={this.onOpenClick.bind(this)}>
                                    Choose File
                                </button>
                            </div>
                            {this.props.photo.length ? <div className="pull-instagram">
                                <a href="/profile/instagram" className="btn btn-primary">
                                    Pull more images from Instagram
                                </a>
                            </div> : null}
                        </div>
                        <div className="file-progress">
                            {this.state.photoStatus[1] !== 0 ? <div>
                            <div>
                                {/*<img src={this.state.photo.preview} width={this.state.photoStatus[1]} height={this.state.photoStatus[2]} />*/}
                                <ReactCrop
                                    src={this.state.photo.preview}
                                    crop={crop}
                                    onComplete={(cropImg) => this.onCropComplete(cropImg)}
                                 />
                            </div>
                            </div> : null}
                        </div>
                        <div className="crop-preview">
                            <img className="crop-img" src="" />
                        </div>
                    </div>

                    <div className="col-xs-8 col-sm-offset-2">
                        <Form
                            onValidationFail = {(state, props) => {
                                console.log('state', state)
                            }}
                            onSubmit = {(values, state, props) => {
                                (this.handleSubmit.bind(this))();
                            }}
                            onChange = {(state, props, initial=false) => {
                                if (Object.keys(state.touched) && Object.keys(state.touched).length) {
                                    (this.handleChange.bind(this, state.values))();
                                }
                            }}
                            defaultValues={{
                                email: this.state.email,
                                firstName: this.state.firstName,
                                lastName: this.state.lastName,
                                location: this.state.location,
                                lang: this.state.lang,
                                gender: this.state.gender
                            }}
                            validate={({ email, firstName, lastName, location, lang, gender }) => {
                              return {
                                email: (!email || !email.match(/^[a-z\.0-9_\-]+@[a-z\.0-9\-]+\.[a-z]+$/i)) ?
                                        'Email is empty or invalid' :
                                        undefined,
                                firstName: (!firstName || !firstName.match(/^[a-z0-9\._\-]+$/i)) ?
                                        'First Name is empty or invalid' :
                                        undefined,
                                lastName: (lastName && lastName.length && !lastName.match(/^[a-z0-9\._\-]+$/i)) ?
                                        'Invalid Last Name' :
                                        undefined,
                                location: (!location || !location.match(/^[a-z0-9\.\- ]+$/i)) ?
                                        'Location is required' :
                                        undefined,
                                lang: (lang && ['en', 'es', 'ru'].indexOf(lang) < 0) ?
                                        'Language is one of en, es, or ru' :
                                        false,
                                gender: (gender && ['f', 'm'].indexOf(gender) < 0) ?
                                        'Invalid gender' :
                                        undefined,
                              }
                            }}
                          >
                            {({ values, props, submitForm, getValue, getError, touched }) => {
                              return (
                                <div className="container">
                                    <div className="error"></div>
                                    <div className="success"></div>
                                    <div className="well well-sm">
                                        <form onSubmit={submitForm}>
                                            <div className="form-group">
                                                <label>Email</label>
                                                <Text
                                                    field='email'
                                                    className="form-control"
                                                />
                                            </div>
                                            <div className="form-group">
                                                <label>First Name</label>
                                                <Text field='firstName' className="form-control" />
                                            </div>
                                            <div className="form-group">
                                                <label>Last Name</label>
                                                <Text field='lastName' className="form-control" />
                                            </div>
                                            <div className="form-group">
                                                <label>Location</label>
                                                <Text field='location' className="form-control" />
                                            </div>
                                            <div className="form-group">
                                                <label>Language</label>
                                                <Text field='lang' className="form-control" />
                                            </div>
                                            <div className="form-group">
                                                <label>Gender</label>
                                                <RadioGroup field='gender'>
                                                  Male <Radio value="m" onChange={()=>{}} /> &nbsp;  &nbsp;
                                                  Female <Radio value="f" onChange={()=>{}} />
                                                </RadioGroup>
                                            </div>
                                            <p></p>
                                            <div className="btn-group">
                                                <button type='submit' className="btn btn-primary">Submit</button>
                                                <button type='reset' className="btn btn-warning">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                    <p></p>
                                    <h1>{`${this.state.firstName} ${this.state.lastName}`} Profile</h1>
                                    <div className="well well-sm">
                                        <div className="row">
                                            <div className='col-xs-4'>
                                                Email
                                            </div>
                                            <div className="col-xs-8">
                                                {this.state.email}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className='col-xs-4'>
                                                First Name
                                            </div>
                                            <div className="col-xs-8">
                                                {this.state.firstName}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className='col-xs-4'>
                                                Last Name
                                            </div>
                                            <div className="col-xs-8">
                                                {this.state.lastName}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className='col-xs-4'>
                                                Location
                                            </div>
                                            <div className="col-xs-8">
                                                {this.state.location}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className='col-xs-4'>
                                                Prefered Language
                                            </div>
                                            <div className="col-xs-8">
                                                {this.state.lang.length == 2 ? this.state.lang : 'en'}
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className='col-xs-4'>
                                                Gender
                                            </div>
                                            <div className="col-xs-8">
                                                {this.state.gender}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              )
                            }}
                        </Form>
                    </div>
                </div>
            )
        }

        return <form></form>
    }
}

export default ProfileEdit;
