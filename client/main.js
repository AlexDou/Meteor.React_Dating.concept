import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'react-mounter';

import App from './components/app';
import requireAuth from './containers/require-auth';
import subProfile from './containers/sub-profile';
import SiteIntro from './components/site/site-intro';
import ProfileEdit from './components/user/profile-edit';
import ProfileConnectInstagram from './components/user/profile-instagram';
import DiscoverRoot from './components/discover/root';
import UserList from './components/discover/user-list';
//import BinsList from './components/bins/bins_list';
//import { Bins } from '../imports/collections/bins';

const EditProfile = requireAuth(subProfile(ProfileEdit));
const Discover = requireAuth(DiscoverRoot);
ProfileConnectInstagram = requireAuth(ProfileConnectInstagram);

FlowRouter.route('/', {
    name: 'root.route',
    action: () => {
        mount(App,
            {content: <SiteIntro />}
        );
      }
});

FlowRouter.route('/profile', {
    name: 'user.profile',
    action: () => {
        mount(App, {
            content: <EditProfile />
        });
    }
});

FlowRouter.route('/profile/instagram', {
    name: 'user.profile.instagram',
    action: () => {
        mount(App, {
            content: <ProfileConnectInstagram />
        });
    }
});

FlowRouter.route('/discover', {
    name: 'discover',
    action: () => {
        mount(App, {
            content:
              <Discover>
                <UserList />
              </Discover>
        });
    }
});

/*const routes = (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={SiteIntro} />
      <Route path="/profile" component={requireAuth(subProfile(ProfileEdit))} />
      <Route path="/discover" component={requireAuth(DiscoverRoot)}>
         <IndexRoute component={UserList} />
      </Route>
    </Route>
  </Router>
);*/

/*Meteor.startup(() => {
  ReactDOM.render(routes, document.querySelector('.app'));
});*/
