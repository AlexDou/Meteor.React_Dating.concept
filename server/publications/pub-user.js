import { Meteor } from 'meteor/meteor';
import Images64 from '../../imports/collections/images64';
import FIELDS from '../../imports/lib/fields';


Meteor.startup(() => {
    Meteor.publish('profile', function() {
      return Meteor.users.find({_id:this.userId}, {fields:FIELDS});
    });

  Meteor.publish('photo', function() {
    return Images64.find({ userId: this.userId });
  });

  /*Meteor.publish('sharedBins', function() {
    const user = Meteor.users.findOne(this.userId);

    if (!user) { return; }

    const email = user.emails[0].address;

    return Bins.find({
      sharedWith: { $elemMatch: { $eq: email }}
    });
});*/
});
