import { Meteor } from 'meteor/meteor';
import Images64 from '../../imports/collections/images64';
import PER_PAGE from '../../imports/lib/per-page';
import FIELDS from '../../imports/lib/fields';

Meteor.publish('user.list', (gender, page) => {
    const users = Meteor.users.find(
        {
            "profile.gender": gender
        },
        {
            skip:((page * PER_PAGE) - PER_PAGE),
            limit: PER_PAGE,
            fields: FIELDS
        });

    return users;
});

Meteor.publish('user.list.photos', photoIds => {
    return Images64.find({_id: { $in: photoIds }});
})
