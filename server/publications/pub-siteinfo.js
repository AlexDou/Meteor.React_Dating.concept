import { Meteor } from 'meteor/meteor';
import SiteInfo from '../../imports/collections/siteinfo';

Meteor.publish('siteinfo', function() {
  return SiteInfo.find({});
});
