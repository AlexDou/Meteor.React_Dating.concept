import { Meteor } from 'meteor/meteor';
import Images64 from '../../imports/collections/images64';
import FIELDS from '../../imports/lib/fields-insta';


Meteor.startup(() => {
    Meteor.publish('profile.instagram.recent', function() {
      return Meteor.users.find({_id:this.userId}, {fields:FIELDS});
    });
});
