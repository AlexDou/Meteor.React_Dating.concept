import Client from 'instagram-private-api/client/v1';
import WebRequest from 'instagram-private-api/client/v1/web/web-request';
import Promise from 'bluebird';
import { EJSON } from 'meteor/ejson';
import path from 'path';
import fs from 'fs';
import _ from 'underscore';

const throwUnauthError = () => {
  throw new Meteor.Error(
    'Unauthorized access',
    'Access not granted',
    'You don\'t have permissions to access this functionality'
  )
}
const throwBadRequestError = () => {
  throw new Meteor.Error(
    'Bad request. Invalid parameter',
    'Bad Request',
    'One of request parameters is invalid'
  )
}

Meteor.methods({
  'profile.instagram': function(jsonCreds) {
    if (!this.userId) {
      return throwUnauthError();
    }

    const user = Meteor.users.findOne(this.userId);

    const creds = EJSON.parse(jsonCreds);
    if (creds.username.length < 3 || !creds.username.match(/^[a-z\._0-9\-]+$/) || creds.password.length < 4) {
      return throwBadRequestError();
    }

    const cookieFile = path.normalize(process.env.PWD + __dirname + '/../cookies/' +this.userId+ '.json');

    if (!fs.existsSync(cookieFile)) {
      fs.writeFileSync(cookieFile, '');
    }

    const device = new Client.Device(this.userId);
    const storage = new Client.CookieFileStorage(cookieFile);
    let imgs = [];

    return Client.Session.create(device, storage, creds.username, creds.password)
        .catch((reason) => {
          console.log(reason)
          return null;
        })
        .then((session) => {
            return session.getAccount()
              .then((account) => {
                return new WebRequest(session)
                    .setMethod('GET')
                    .setUrl(`https://www.instagram.com/${account.params.username}/`)
                    .generateUUID()
                    .signPayload()
                    .send()
                    .then((data) => {
                        let sharedData_str = _.object(['junk', 'sData'], data.body.split('window._sharedData')).sData;
                        sharedData_str = sharedData_str
                                          .replace(/^[ ]?=[ ]?/i, '')
                                          .replace(/;[ ]?<\/script>[\s\S]*/im, '')
                                          .replace(/[\s\S]*"media": \{/, '')
                                          .replace(/}}]}[\s\S]*/, '');

                        const thumb_src_r = sharedData_str.match(/"thumbnail_src": "([^"]+)"/g);

                        if (typeof thumb_src_r === 'object') {
                          thumb_src_r.map((img_str) => {
                            imgs.push(img_str.replace(/thumbnail_src/,'').replace(/"/g,'').replace(/: /g,''))
                          })
                        }

                        return imgs
                    })
                })
        })
        .catch((reason2) => {
          console.log(reason2)
          return null;
        });
  }
});

Meteor.methods({
    'update.recent.instagram': (jsonImgs) => {
        const user = Meteor.users.findOne(Meteor.userId());
        const profile_new = Object.assign(user.profile, { insta: jsonImgs });
        return Meteor.users.update(
            {_id:Meteor.userId()},
            { $set:
                {
                    profile: profile_new
                }
            }
        );
    }
})
