import Images64 from '../../imports/collections/images64';
import { EJSON } from 'meteor/ejson';

const throwUnauthError = () => {
  throw new Meteor.Error(
      //error
      'Unauthorized access',
      //reason
      'Access not granted',
      //details
      'You don\'t have permissions to access this functionality'
  )
}

Meteor.methods({
  'profile.remove': function() {
    if (this.userId) {
      const user = Meteor.users.find(this.userId);
      if (user.profile.photoId) {
          Images64.remove(user.profile.photoId);
      }
      Meteor.users.remove(this.userId)
    }

    return throwUnauthError();
  },

  'profile.update': function(jsonUser) {
      if (this.userId) {
          let photoId = null;
          const user = EJSON.parse(jsonUser);
          const userPhoto = Images64.find({ userId: this.userId }).fetch();
          //console.log('user: ', user);
          //console.log('userPhoto: ', userPhoto);

          if (user.photo && user.photo.length > 100) {
            if (!userPhoto.length) {
              photoId = Images64.insert({
                userId: this.userId,
                photo: user.photo
              });
            }
            else {
              Images64.update(
                {userId: this.userId},
                { $set: {
                  photo: user.photo
                }}
              );
              photoId = userPhoto[0]['_id'];
            }
          }
          const profile_new = {
            'first-name': user.firstName,
            'last-name': user.lastName,
            'photoId': photoId,
            'location': user.location,
            'lang': user.lang,
            'gender': user.gender
          }
          return Meteor.users.update(
              {_id:this.userId},
              { $set:
                  {
                      "emails.0.address": user.email,
                      "profile": profile_new
                  }
              }
          );
      }

      return throwUnauthError();
  }
});
