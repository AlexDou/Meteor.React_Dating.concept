import SiteInfo from '../imports/collections/siteinfo';

Meteor.startup(function(){
    const siteInfo_count = SiteInfo.find({}).count();
    if (!siteInfo_count) {
        SiteInfo.insert({
            'title': 'Welcome to Teezdom Come dating',
            'domain': 'teezdom.com',
            'intro': "At vero eos et accusamus et iusto odio dignissimos\
             ducimus qui blanditiis praesentium voluptatum deleniti atque\
             corrupti quos dolores et quas molestias excepturi sint occaecati\
             cupiditate non provident, similique sunt in culpa qui officia\
             deserunt mollitia animi, id est laborum et dolorum fuga."
        })
    }
});
